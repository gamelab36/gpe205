﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour
{
    //PUBLIC VARIABLES.
    public Rigidbody Bullet; // VARIABLE TO HOLD THE BULLET TO FIRE FROM TANK DESIGNER CAN SELECT DIFERENT BULLETE WITHOUT EDITING CODE. 
    public Transform Firetransform; // THIS LINE OF CODE IS TO SELET THE FIRE TRANSFORM DESIGNER CAN SELECT HEIT OWN LOCATION TO FIRE THE BULLET.
    public float FireSpeed = 20.0f; // THIS VARIABLE IS FOR TO SET FIRE SPEED DESIGNER CAN CHANGE THIS NUMBER IN EDITOR HOW FAST OR SLOW THEY WANT TO FIRE.
    public float firedelay = 3.0f; // THIS VARIABLE IS TO SET HTE DELAY BETWEEN EACH FIRE.

    //PRIVATE VARIABLES.
    private bool p_Fired = true; // VAIABLE TO CHECK IF PLAYER HAS FIRED YET 
    private float nextEventTime; //

    // Start is called before the first frame update
    void Start()
    {
        nextEventTime = Time.time + firedelay;
    }

    private void Delay()
    {
        if (Time.time >= nextEventTime)
        {
            p_Fired = true; // SETTING TO TRUE AFTER 1 SECOND ON THE FIRE DELAY TIME.
            nextEventTime = Time.time + firedelay;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Delay(); //CALLS THE DELAY FUNCTIONS 
        if(p_Fired) // IT CHECK'S IF BULLETE IS FIRED OR NOT IF ITS TRUE CALLS FIRE FUNCTION. 
        {
           // Fire(); // CALLS THE FIRE FUNCTION IF THE p_FIRED IS TRUE.

        } // ENDING IF STATEMENT.
    } //END OF UPDATE FUNCTION

    public void Fire()
    {
        p_Fired = false; // SETTING FIRE TO FALSE WHEN PLAYER FIRES THE BULLETE.


        Rigidbody bulletInstance = Instantiate(Bullet, Firetransform.position, Firetransform.rotation) as Rigidbody; // THIS LINE OF CODE IS INSTANTIATING THE BULLETE AS A ROGODBODY AND SETTING THE FIRE POSITION AND ROTATION.

        bulletInstance.velocity = FireSpeed * Firetransform.forward; // IN THIS LINE THE FIRE SPEED AND IT IS BEING MULTIPLIED WITH THE FIRETRANSFORM FORWARD DIRECTION. 
    } // END OF FIRE FUNCITION.
}
