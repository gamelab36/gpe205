﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{

    public float e_CurrentHealth = 0.0f; // THIS VARIABLE HOLDS THE ENMEY'S CURRENT HEALTH
    public float e_MaxHealth = 100.0f; // THIS LINE HOLDS THE ENEMY'S MAX HEALTH.
    public TankData Tank; // HOLDS THE tANK DATA SCRIPT.
    public int e_Score = 0; // HOLDS THE ENEMY SCORE VALUE.
    public int scoreAmt ;


    // Start is called before the first frame update
    void Start()
    {
        e_CurrentHealth = e_MaxHealth; // SET THE CURRENT HEALTH TO MAX WHEN THE GAME STARTS.
        Tank = gameObject.GetComponent<TankData>();
    }

    // Update is called once per frame
    void Update()
    {
        if (e_CurrentHealth <= 0) // CHECK IF THE ENEMY HEALTH IS LESS THAN OR EQUAL TO 0. IF TIT'S TRUE ADD THE 1 SCORE FOR THE PLAYER AND DESTROY THE ENEMY. 
        {
            Tank.AddScore(scoreAmt); // IF THE ENEMY HEALTH IS 0 THIS LINE WILL ADD SCORE TO THE PLAYER.

            Destroy(gameObject); // DESTROY THE ENEMY GAME OBJECT IF THE HEALTH IS 0.
        }

        
    }

    public void DamageEnemy(float damageAmt) // THIS FUNCTION WILL DAMAGE ENEMY
    {
        e_CurrentHealth -= damageAmt; // THIS LINE OF CODE WILL REDUCE THE HEALTH OF ENEMY 
    }


    public void AddScore(int ScoreAmt) // THIS SCORING FUNCTION IS FOR THE ENEMY. 
    {
        e_Score += ScoreAmt; // THIS LINE WILL ADD THE ENEMY SCORE IF PLAYER DIES.
    }
}
