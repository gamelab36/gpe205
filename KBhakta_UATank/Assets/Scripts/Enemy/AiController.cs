﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiController : MonoBehaviour
{

    public enum AIState { Chase, ChaseAndFire, CheckForFlee, Flee, Rest, Patrol }; // This line of code hold all the personalities of AI. 
    public AIState aiState = AIState.Chase; // This line holds the Variable AI state.
    public float stateEnterTime; //This variable keep track of enter time.
    public float aiSenseRadius; // AI Sense radius.
    public float restingHealRate; // In hp/second 
    public EnemyScript enemyScript; // Holds Enemy Script
    public Transform target; // Target to chase
    public Transform tf; // Transform
    public float avoidanceTime = 2.0f;
    public EnemyWeapon enemyWeapon;  //Holds Enemy Weapon script.
    public float lastShootTime; 
    public float fleeDistance = 1.0f; 
    public Transform[] wayPoints; // Arrays of Way points.
    public float closeEnough = 1.0f; 
    public LoopType loopType;
    public enum LoopType  //lop enum for designers to let enemy stop at last way point or make the keep patrolling or back and forth on two way points.
    {
        Stop,
        Loop,
        PingPong
    };

    private int avoidanceStage = 0; 
    private TankMotor motor; // Holds the script of Tank Motor
    private TankData data; //Holds the script of Tank Data.
    private float exitTime; 
    private int currentWaypoint = 0; 
    private bool isPatrolForward = true; 


    public void Awake()
    {
        tf = gameObject.GetComponent<Transform>();
       
    }

   





    // Start is called before the first frame update
    void Start()
    {
        data = gameObject.GetComponent<TankData>(); //SETTING TANKDATA ON START

        motor = gameObject.GetComponent<TankMotor>(); // SETTING TANKMOTOE ON START.
    }



    public void CheckForFlee()
    {
        // TODO: Write the CheckForFlee state.
    }

    public void DoRest()
    {
        // Increase our health. Remember that our increase is "per second"!
        enemyScript.e_CurrentHealth += restingHealRate * Time.deltaTime;

        // But never go over our max health
        enemyScript.e_CurrentHealth = Mathf.Min(enemyScript.e_CurrentHealth, enemyScript.e_MaxHealth);
    }

    public void ChangeState(AIState newState)
    {

        // Change our state
        aiState = newState;

        // save the time we changed states
        stateEnterTime = Time.time;
    }

    public void PlayerisDead()
    {
        ChangeState(AIState.Rest);
    }


                                                                                                         // CANMOVE FUNCTION.




    public bool CanMove(float speed)
    {
        // Cast a ray forward in the distance that we sent in
        RaycastHit hit;

        // If our raycast hit something...
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {
            // ... and if what we hit is not the player...
            if (!hit.collider.CompareTag("Player"))
            {
                // ... then we can't move
                return false;
            }
        }
        // otherwise, we can move, so return true
        return true;
    }


                                                                                                        // CHASE FUNCTION.
    
    void DoChase()
    {     
       motor.RotateTowards(target.position, data.turnSpeed);  // Rotate towards the target     
                                                              
        if (CanMove(data.moveSpeed))
        {
            motor.Move(data.moveSpeed);
        }
        else
        {
            // Enter obstacle avoidance stage 1
            avoidanceStage = 1;
        }
    }


                                                                                                      // AVOIADANCE FUNCTION


    void DoAvoidance()
    {
        if (avoidanceStage == 1)
        {
            // Rotate left
            motor.Rotate(-1 * data.turnSpeed);

            // If I can now move forward, move to stage 2!
            if (CanMove(data.moveSpeed))
            {
                avoidanceStage = 2;

                // Set the number of seconds we will stay in Stage2
                exitTime = avoidanceTime;
            }

            // Otherwise, we'll do this again next turn!
        }
        else if (avoidanceStage == 2)
        {
            // if we can move forward, do so
            if (CanMove(data.moveSpeed))
            {
                // Subtract from our timer and move
                exitTime -= Time.deltaTime;
                motor.Move(data.moveSpeed);

                // If we have moved long enough, return to chase mode
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            }
            else
            {
                // Otherwise, we can't move forward, so back to stage 1
                avoidanceStage = 1;
            }
        }

    }



                                                                                                     //FLEE FUNCTION.
        

    void DoFlee()
    {
        if (aiState == AIState.Flee)
        {

            Vector3 vectorToTarget = target.position - tf.position; // The vector from AI to target is target position minus our position.
            


            Vector3 vectorAwayFromTarget = -1 * vectorToTarget;  // We can flip this vector by -1 to get a vector AWAY from our target


            vectorAwayFromTarget.Normalize();  // Now, we can normalize that vector to give it a magnitude of 1


            vectorAwayFromTarget *= fleeDistance;   // A normalized vector can be multiplied by a length to make a vector of that length.

            // We can find the position in space we want to move to by adding our vector away from our AI to our AI's position.
            //     This gives us a point that is "that vector away" from our current position.
            Vector3 fleePosition = vectorAwayFromTarget + tf.position;
            motor.RotateTowards(fleePosition, data.turnSpeed);
            motor.Move(data.moveSpeed);
        }
    }



                                                                                                 //PATROL FUNCTION.

                                                                        //IF DESIGNER SELECT THIS STATE THEY HAVE TO ASSIGN A WAYPOINT TO AI. 


    void DoPatrol()
    {
        if(aiState == AIState.Patrol)
        {
            if (motor.RotateTowards(wayPoints[currentWaypoint].position, data.turnSpeed))
            {
                //DoNothing. 
            }
            else
            {
                motor.Move(data.moveSpeed);
            }

            if (Vector3.SqrMagnitude(wayPoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))
            {
                if (loopType == LoopType.Stop)                                          // IF DESIGNER SELECT THE AI MODS FOR PATROL STATE. AI WILL STOP AT THE LAST WAYPOINTS.
                {
                    if (currentWaypoint < wayPoints.Length - 1)
                    {
                        currentWaypoint++;
                    }
                }
                else if (loopType == LoopType.Loop)                                     // IF DESIGNRE SELECT LOOP STATE FOR PATROL AI WILL LAWAYS ALWAYS LOOPS BETWEEN.
                {
                    if (currentWaypoint < wayPoints.Length - 1)
                    {
                        currentWaypoint++;
                    }
                    else
                    {
                        currentWaypoint = 0;
                    }
                }

                else if (loopType == LoopType.PingPong)                                 // IN THIS STATE AI WILL GO BACK AND FORTH BETWEEN TWO TARGETS.
                {
                    if (isPatrolForward)
                    {
                        if (currentWaypoint < wayPoints.Length - 1)
                        {
                            currentWaypoint++;
                        }
                        else
                        {
                            isPatrolForward = false;
                            currentWaypoint--;
                        }
                    }
                }
                else
                {
                    if (currentWaypoint > 0)
                    {
                        currentWaypoint--;
                    }
                    else
                    {
                        isPatrolForward = true;
                        currentWaypoint++;
                    }
                }
            }
        }
    }







    // Update is called once per frame
    void Update()
    {
        //IF PLAYER IS DEAD CHANGET THE AI STATE TO FLEE.
        if (data.isPlayerDead)
        {
            
            PlayerisDead();
        }

        
        if (aiState == AIState.Chase)                                                                           //CHECKS IF AI STATE IS IN CHASE
        {
            // Perform Behaviors
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoChase();
            }

            // Check for Transitions
            if  (enemyScript.e_CurrentHealth < enemyScript.e_CurrentHealth * 0.5f)
            {
                ChangeState(AIState.CheckForFlee);                                                             
            }
            else if (Vector3.Distance(target.position, tf.position) <= aiSenseRadius)
            {
                ChangeState(AIState.ChaseAndFire);
            }

            else if(data.isPlayerDead)
            {
                DoFlee();
            }
        }
        else if (aiState == AIState.ChaseAndFire)                                                             //CHECKS IF AI STATE IS EQUALS TO CHASE AND FIRE.
        {
            // Perform Behaviors
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                
                    DoChase();
                

                // Limit our firing rate, so we can only shoot if enough time has passed
                if (Time.time > lastShootTime + enemyWeapon.firedelay)
                {
                    enemyWeapon.Fire();
                    lastShootTime = Time.time;
                }
            }
            // Check for Transitions
            if (enemyScript.e_CurrentHealth < enemyScript.e_MaxHealth * 0.5f)
            {
                ChangeState(AIState.CheckForFlee);
            }
            else if (Vector3.Distance(target.position, tf.position) > aiSenseRadius)
            {
                ChangeState(AIState.Chase);
            }
        }
        else if (aiState == AIState.Flee)                                                           //CHECKS IF AI STATE IS EQUALS TO FLEE
        {
            // Perform Behaviors
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoFlee();
            }

            // Check for Transitions
            if (Time.time >= stateEnterTime + 30)
            {
                ChangeState(AIState.CheckForFlee);
            }
        }
        else if (aiState == AIState.CheckForFlee)                                                 //CHECKS IF AI STATE IS EQUALS TO CHECK OR FLEE
        {
            // Perform Behaviors
            CheckForFlee();

            // Check for Transitions
            if (Vector3.Distance(target.position, tf.position) <= aiSenseRadius)
            {
                ChangeState(AIState.Flee);
            }
            else
            {
                ChangeState(AIState.Rest);
            }
        }
        else if (aiState == AIState.Rest)                                                           //CHECKS IF AI STATE IS EQUALS REST
        {
            // Perform Behaviors
            DoRest();

            // Check for Transitions
            if (Vector3.Distance(target.position, tf.position) <= aiSenseRadius)
            {
                ChangeState(AIState.Flee);
            }
            else if (enemyScript.e_CurrentHealth >= enemyScript.e_MaxHealth)
            {
                ChangeState(AIState.Chase);
            }
        }
        else if (aiState == AIState.Patrol)                                                         //CHECKS IF AI STATE IS EQUALS TO PATROL
        {
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoPatrol();
            }

        }
        else if (data.isPlayerDead)                                                                 //CHECKS IF PLAYER IS DEAD
        {
            PlayerisDead();
        }

        





    }
}
