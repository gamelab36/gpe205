﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletScript : MonoBehaviour
{
    public float destroyTime = 1.0f;  //HOLDS THE VALUE FOR THE DELAY TIME.
    public float applyDamage = 25.0f; // HOLDS THE VALUE FOR THE DAMAGE AMOUNT FOR PLAYER.



    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, destroyTime); // THIS WILL DESTROY THE BULLETE AFTER 1 SECOND.
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)  // THIS FUNCTION CHECKS IF THE BULLETE IS ENTERING THE PLAYER COLLISION.
    {
       

        if (collision.transform.tag == "Player") // THIS LINE CHECKS THE HITED OBJECT HAS PLAYER TAG OR NOT 
        {
            if (collision.collider.GetComponent<TankData>() != null) // CHECKS IF THE COLLISION IS COLLIDING WITH THE PLAYER AND CHECK IF ITS NOT NULL IT WILL DAMAGE PLAYER.
            {
                collision.collider.GetComponent<TankData>().DamagePlayer(applyDamage); // THIS LINE WILL DAMAGE PLAYER IF THE PLAYER TAG IS TRUE AND ITS NOT NULL.
            }
            Destroy(gameObject); // THIS LINE WILL DESTROY ENEMY BULLETE IF ITS HITS OBJECT WITH PLAYER TAG.
        }

    }
}
