﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour
{


    //PUBLIC VARIABLES.
    public Rigidbody Bullet; // VARIABLE TO HOLD THE BULLET TO FIRE FROM TANK DESIGNER CAN SELECT DIFERENT BULLETE WITHOUT EDITING CODE.
    public string p_FireButton; // THIS VARIABLE HOLD THE FIRE BUTTON INPUT AND DESIGNER CAN SELECT THER OWN IF THEY WANT TO. 
    public Transform Firetransform; // THIS LINE OF CODE IS TO SELET THE FIRE TRANSFORM DESIGNER CAN SELECT HEIT OWN LOCATION TO FIRE THE BULLET.
   // public float FireSpeed = 20.0f; // THIS VARIABLE IS FOR TO SET FIRE SPEED DESIGNER CAN CHANGE THIS NUMBER IN EDITOR HOW FAST OR SLOW THEY WANT TO FIRE.
  //  public float firedelay = 3.0f; // THIS VARIABLE IS TO SET HTE DELAY BETWEEN EACH FIRE.
    public bool p_Fired = true; // VAIABLE TO CHECK IF PLAYER HAS FIRED YET 


    //PRIVATE VARIABLES.
    private float nextEventTime; // 
    public TankData data;
  

    


    // Start is called before the first frame update
    void Start()
    {
        p_FireButton = "PlayerFire"; // SETTING DEFAULT FIRE BUTTON. 

        nextEventTime = Time.time + data.firedelay;

        data = gameObject.GetComponent<TankData>();
        
    }

    // Delay function so player cannot fire rapidly.
    private void Delay()
    {
        if (Time.time >= nextEventTime)
        {
           // Debug.Log("CanFire");

            p_Fired = true; // SETTING TO TRUE AFTER BASED ON THE FIRE DELAY TIME.


            nextEventTime = Time.time + data.firedelay;
        }
    }



    // Update is called once per frame
    void Update()
    {

        Delay(); // Calling Delay function to check if player can fire or not.

        if(Input.GetButtonDown(p_FireButton) && p_Fired) // THIS LINE OF CODE CHECK'S IF FIRE BUTTON IS PRESSED.
        {
            if(data.isPlayerDead != true)
            {
                Fire(); // HERE THE FIRE FUNCTION IS CALLED IF p_FIRED IS TRUE.
            }
        }

        
    }

    


    // FIRE FUNCTION 
    private void Fire()
    {
        p_Fired = false; // SETTING FIRE TO FALSE WHEN PLAYER FIRES THE BULLETE.


        Rigidbody bulletInstance = Instantiate(Bullet, Firetransform.position, Firetransform.rotation) as Rigidbody; // THIS LINE OF CODE IS INSTANTIATING THE BULLETE AS A ROGODBODY AND SETTING THE FIRE POSITION AND ROTATION.

        bulletInstance.velocity = data.FireSpeed * Firetransform.forward; // IN THIS LINE THE FIRE SPEED AND IT IS BEING MULTIPLIED WITH THE FIRETRANSFORM FORWARD DIRECTION. 
    }
}
