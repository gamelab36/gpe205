﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{
    public float moveSpeed = 3; // HOLDS THE MOVE SPEED OF PLAYER TANK
    public float turnSpeed = 180; // HOLDS THE TURN SPEED OF PLAYER TANK
    public int p_Score = 0; // HOLDS THE PLAYER SCORE.
    public float p_CuttentHealth = 0.0f; // THIS VARIABLE IS FOR THE PLAYER CURRENT HEALTH.
    public float p_MaxHealth = 100.0f; // IT HOLDS THE MAX HEALTH OF THE ENEMY.
    public int scoreAmt;
    public bool isPlayerDead;
    public float FireSpeed; // THIS VARIABLE IS FOR TO SET FIRE SPEED DESIGNER CAN CHANGE THIS NUMBER IN EDITOR HOW FAST OR SLOW THEY WANT TO FIRE.
    public float firedelay; // THIS VARIABLE IS TO SET HTE DELAY BETWEEN EACH FIRE.

    private EnemyScript eScript; //HOLDS THE ENEMY SCRIPT.
    private PlayerWeapon playerweapon;
    public AiController aIController;
    // Start is called before the first frame update
    void Start()
    {
        p_CuttentHealth = p_MaxHealth; // SET THE CURRENT HEALTH TO MAX WHEN THE GAME STARTS.

        eScript = gameObject.GetComponent<EnemyScript>();

        aIController = gameObject.GetComponent<AiController>();

        FireSpeed = 20.0f;
        firedelay = 2.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (p_CuttentHealth <= 0) // CHECK IF THE PLAYER HEALTH IS LESS THAN OR EQUAL TO 0. IF TIT'S TRUE ADD THE 1 SCORE FOR THE ENEMY AND DESTROY THE PLAYER.
        {
            isPlayerDead = true;

            // eScript.AddScore(scoreAmt); // IF THE PLAYER HEALTH IS 0 THIS LINE WILL ADD SCORE TO THE ENEMY.

        }
        if (isPlayerDead)
        {
           // aIController.PlayerisDead();

            turnSpeed = 0.0f;
            moveSpeed = 0.0f;
           // playerweapon.p_Fired = false;
           // Destroy(gameObject); // DESTROY THE PLAYER GAME OBJECT IF THE HEALTH 0.

        }
    }


    public void AddScore(int ScoreAmt) // THIS SCORING FUNCTION IS FOR THE PLAYER. 
    {
        p_Score += ScoreAmt; // THIS LINE WILL ADD THE PLAYER SCORE IF ENEMY DIES.
    }

    public void DamagePlayer(float dmgAmt) // THIS FUNCTION WILL DAMAGE PLAYER.
    {
        p_CuttentHealth -= dmgAmt; // THIS LINE OF CODE WILL REDUCE THE HEALTH OF ENEMY.
    }


    

}
