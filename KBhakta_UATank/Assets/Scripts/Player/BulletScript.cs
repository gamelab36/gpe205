﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{

    public float destroyTime = 1.0f; //HOLDS THE VALUE FOR THE DELAY TIME.
    public float applyDamage = 50.0f; // HOLDS THE VALUE FOR THE DAMAGE AMOUNT FOR ENEMY.
   
    

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, destroyTime); // THIS WILL DESTROY THE BULLETE AFTER 1 SECOND.
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    
    // THIS FUNCTION CHECKS IF THE BULLETE IS ENTERING THE ENEMY COLLISION.
    private void OnCollisionEnter(Collision collision) 
    {

        if(collision.transform.tag == "Enemy") // THIS LINE CHECKS THE HITED OBJECT HAS ENEMY TAG OR NOT.
        {
            if (collision.collider.GetComponent<EnemyScript>() != null) // CHECKS IF THE COLLISION IS COLLIDING WITH THE ENEMY AND CHECK IF ITS NOT NULL IT WILL DAMAGE ENEMY.
            {
                collision.collider.GetComponent<EnemyScript>().DamageEnemy(applyDamage); // THIS LINE WILL DAMAGE ENEMY IF THE ENEMY TAG IS TRUE AND ITS NOT NULL.
            }
            Destroy(gameObject); // THIS LINE WILL DESTROY PLAYER BULLETE IF ITS HITS OBJECT WITH ENEMY TAG.
        }
       
    }
}
