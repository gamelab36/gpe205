﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Powerup 
{
    public float speedModifier;
    public float healthModifier;
    public float maxHealthModifier;
    public float fireRateModifier;
    public float duration;
    public bool isPermanent;



    public void OnActivate(TankData target)
    {
        target.moveSpeed += speedModifier;
        target.p_CuttentHealth += healthModifier;
        target.p_MaxHealth += maxHealthModifier;
        target.firedelay -= fireRateModifier;
    }

    public void OnDeactivate(TankData target)
    {
        target.moveSpeed -= speedModifier;
        target.p_CuttentHealth -= healthModifier;
        target.p_MaxHealth -= maxHealthModifier;
        target.firedelay += fireRateModifier;
    }

}
