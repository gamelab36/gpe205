﻿using UnityEngine;
using System.Collections;

public class Room : MonoBehaviour {

    //PUBLIC VARIABLES.
	public GameObject doorNorth;
	public GameObject doorSouth;
	public GameObject doorEast;
	public GameObject doorWest;
    public GameObject[] gridPrefabs;
    public int rows;
    public int cols;

    //PRIVATE VARIABLES.
    private float roomWidth = 50.0f;
    private float roomHeight = 50.0f;
    private Room[,] grid;

    
    void Start()
    {
        
        GenerateGrid();
    }

    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[Random.Range(0, gridPrefabs.Length)];
    }

    public void GenerateGrid()
    {
      
        grid = new Room[rows,cols]; 

        
        for (int i = 0; i < rows; i++) {
          
           for (int j = 0; j < cols ; j++) {
              
              float xPosition = roomWidth * j;
              float zPosition = roomHeight * i;
              Vector3 newPosition =  new Vector3 (xPosition, 0.0f, zPosition);

              
              GameObject tempRoomObj = Instantiate (RandomRoomPrefab(), newPosition, Quaternion.identity) as GameObject;

             
              tempRoomObj.transform.parent = this.transform;

              
              tempRoomObj.name = "Room_"+j+","+i;

            
              Room tempRoom = tempRoomObj.GetComponent<Room>();

            
             
              if (i == 0) {
                 tempRoom.doorNorth.SetActive(false);
              } 
              else if ( i == rows-1 ){
               
                 tempRoom.doorSouth.SetActive(false);
              }
              else {
                
                 tempRoom.doorNorth.SetActive(false);
                 tempRoom.doorSouth.SetActive(false);
              }
             
              if (j == 0) {
                  tempRoom.doorEast.SetActive(false);
              } 
              else if ( j == cols-1 ){
                
                 tempRoom.doorWest.SetActive(false);
              }
              else {
                
                 tempRoom.doorEast.SetActive(false);
                 tempRoom.doorWest.SetActive(false);
              }
             
              grid[j,i] = tempRoom;
           }
        }
     }

    
     
}



